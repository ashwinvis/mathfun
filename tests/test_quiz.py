from hypothesis import given
import hypothesis.strategies as st

import math
from mathfun.quiz import two_numbers


@given(st.integers(), st.integers())
def test_two_numbers(a, b):
    m, n = two_numbers(a, b, big_then_small=True)
    j, k = min(a, b), max(a, b)
    assert j <= m <= k
    assert j <= n <= k
    assert m >= n
