from hypothesis import given
import hypothesis.strategies as st

import math
from mathfun.text import in_words, units, scales


@given(st.integers(min_value=0, max_value=int(1e15)))
def test_in_words(x):
    result = in_words(x)
    assert isinstance(result, str)
    assert len(result) > 0

    if x:
        assert "zero" not in result

    if x < 20:
        assert any(word in result for word in units)
    elif x >= 1000:
        digits = int(math.log10(x))
        order = scales[2 + digits // 3]
        assert order in result
