import random
from operator import add, sub

from rich.console import Console
from rich.markdown import Markdown
from rich.columns import Columns


ops = {"+": add, "-": sub}
alphabets = "fghjk"
console = Console()


def two_numbers(a=0, b=10, big_then_small=True):
    lims = sorted((a, b))
    rand_nbs = (random.randint(*lims), random.randint(*lims))
    return sorted(rand_nbs, reverse=True) if big_then_small else rand_nbs


def reward():
    msg = random.choice(
        (
            "Good job 🤩",
            "Correct answer 😌",
            "You're doing great 🤠",
            "Well done 😄",
            "Yay 🥳",
            "That's correct 😎"
        )
    )
    console.print(msg)


def try_again():
    msg = random.choice(
        (
            "Oopsie daisy ⁉",
            "Try again 🙈",
            "Whoops 🤭"
        )
    )
    console.print(msg)


def ask(symbol: str, a: int, b: int):
    """Ask an arithemetic question operating on two numbers."""
    op = ops[symbol]

    correct_ans = op(a, b)
    choices = [correct_ans, op(*two_numbers(a, b)), op(*two_numbers(a, b))]
    random.shuffle(choices)

    question = Markdown(f"# What is {a} {symbol} {b}?")
    options = Columns(f"({a}) {choice}    " for a, choice in zip(alphabets, choices))
    nb_options = len(choices)

    console.print(question)
    console.print(options)

    err_msg = (
        "Expected a number or an alphabet: "
        f"{', '.join(alphabets[:nb_options])}. "
        "Try again."
    )
    while (opt := (input("Answer? ") or " ")):
        if (
            not opt.isdigit() and (
                not opt.isalpha() or
                len(opt) > 1 or
                opt not in alphabets or
                alphabets.index(opt) > nb_options
            )
        ):
            console.print(err_msg)
            continue

        if opt.isdigit():
            ans = int(opt)
        else:
            idx_opt = alphabets.index(opt)
            ans = choices[idx_opt]

        if ans == correct_ans:
            reward()
            return 1
        else:
            try_again()
            console.print(f"You chose {ans}. It should have been {correct_ans}")
            return 0


if __name__ == "__main__":
    nb_rounds = 5
    points = 0
    nb_min = 0
    nb_max = 10
    symbols = tuple(ops)
    for i in range(nb_rounds):
        points += ask(random.choice(symbols), *two_numbers())
