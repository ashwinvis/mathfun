# singles
units = (
    "zero",
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine",
    "ten",
    "eleven",
    "twelve",
    "thirteen",
    "fourteen",
    "fifteen",
    "sixteen",
    "seventeen",
    "eighteen",
    "nineteen",
)

# tens
tens = (
    "",
    "",
    "twenty",
    "thirty",
    "forty",
    "fifty",
    "sixty",
    "seventy",
    "eighty",
    "ninety",
)

# larger scales
scales = ("", "", "hundred", "thousand", "million", "billion", "trillion")


def place(x: int, divisor: int, digits: int = 1):
    xs = str(x // divisor)
    s = slice(-min(len(xs), digits), None)
    return int(xs[s])


def in_words(x: int):
    """Transliterate integer to English."""
    if x < 0:
        raise NotImplementedError("Negative numbers not supported, yet.")
    elif x >= int(1e15):
        raise NotImplementedError("Numbers starting from 1 quadrillion not supported, yet.")
    elif x < 20:
        return units[x]
    else:
        x_ones = place(x, 1)
        x_tens = place(x, 10)
        x_hund = place(x, 100)

        answer = []

        x_scales = [place(x, 10 ** exp, 3) for exp in range(3, 13, 3)]
        for word, xs in zip(scales[::-1], x_scales[::-1]):
            if xs:
                answer.extend([in_words(xs), word])

        if x_hund:
            answer.extend([in_words(x_hund), scales[2]])

        if x_tens or x_ones:
            if answer:
                answer.append("and")

            x_ones2 = place(x, 1, 2)
            if x_ones2 < 20:
                answer.append(in_words(x_ones2))
            else:
                answer.append(tens[x_tens])

                if x_tens > 1 and x_ones > 0:
                    answer.append("-")

                # Do not append zero if answer contains some words
                if not (answer and x_ones == 0):
                    answer.append(units[x_ones])

        # White space and punctuations
        answer = " ".join(answer).replace("  ", " ").replace(" - ", "-")
        for scale in scales[3:]:
            answer = answer.replace(f"{scale} ", f"{scale}, ")

        return answer

if __name__ == "__main__":
    import random
    import itertools

    for _ in range(10):
        x = random.randint(0, 10**random.randint(1, 12))
        w = in_words(x)
        print(f"{x:12d} = {w}")
