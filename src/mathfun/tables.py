import itertools
from typing import Generator

from rich.console import Console
from rich.table import Table

from .text import in_words


def mul_gen(x: int, start: int, step: int) -> Generator[int, str, str]:
    """Generate data for a multiplication table."""
    for number in itertools.count(start, step):
        answer = number * x
        words = in_words(answer)
        same_as = " + ".join(str(x) for _ in range(number))
        yield (f"{number} ⨉ {x}", answer, words, same_as)


def mul_table(x: int = 2, start: int = 0, step: int = 1, stop: int = 10):
    table = Table(show_header=True, header_style="bold magenta")
    table.add_column(f"_ times {x}", justify="right")
    table.add_column("=", justify="right")
    table.add_column("in words", style="dim")
    table.add_column("is same as")

    for items in mul_gen(x, start, step):
        table.add_row(*(str(i) for i in items))
        if items[1] == x * stop:
            break

    console = Console()
    console.print(table)


if __name__ == "__main__":
    import sys

    assert len(sys.argv) == 2 and float(x := sys.argv[1]).is_integer()

    mul_table(int(x))
