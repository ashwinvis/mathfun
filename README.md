# Math(ematical) Fun(ctions)

An educational package with utilities to teach basic math on the console.

## Installation

Setup a [python virtual environment][venv] and then,

```sh
pip install https://codeberg.org/ashwinvis/mathfun/archive/main.tar.gz
```

[venv]: https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment

## Features

### Multiplication tables

Generate multiplication tables of any integer and show how it is nothing but
reduced addition. The table is displayed with colours using the `rich` library.

```sh
$ python -m mathfun.tables 9
┏━━━━━━━━━━━┳━━━━┳━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ _ times 9 ┃  = ┃ in words     ┃ is same as                            ┃
┡━━━━━━━━━━━╇━━━━╇━━━━━━━━━━━━━━╇━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┩
│     0 ⨉ 9 │  0 │ zero         │                                       │
│     1 ⨉ 9 │  9 │ nine         │ 9                                     │
│     2 ⨉ 9 │ 18 │ eighteen     │ 9 + 9                                 │
│     3 ⨉ 9 │ 27 │ twenty-seven │ 9 + 9 + 9                             │
│     4 ⨉ 9 │ 36 │ thirty-six   │ 9 + 9 + 9 + 9                         │
│     5 ⨉ 9 │ 45 │ forty-five   │ 9 + 9 + 9 + 9 + 9                     │
│     6 ⨉ 9 │ 54 │ fifty-four   │ 9 + 9 + 9 + 9 + 9 + 9                 │
│     7 ⨉ 9 │ 63 │ sixty-three  │ 9 + 9 + 9 + 9 + 9 + 9 + 9             │
│     8 ⨉ 9 │ 72 │ seventy-two  │ 9 + 9 + 9 + 9 + 9 + 9 + 9 + 9         │
│     9 ⨉ 9 │ 81 │ eighty-one   │ 9 + 9 + 9 + 9 + 9 + 9 + 9 + 9 + 9     │
│    10 ⨉ 9 │ 90 │ ninety       │ 9 + 9 + 9 + 9 + 9 + 9 + 9 + 9 + 9 + 9 │
└───────────┴────┴──────────────┴───────────────────────────────────────┘
```

### Numbers to text

Convert numbers to their text equivalent. Works for any integer until order of
trillions. This example code converts some random numbers in the range.

```sh
$ python -m mathfun.text
 79503262279 = seventy-nine billion, five hundred and three million, two hundred and sixty-two thousand, two hundred and seventy-nine
          18 = eighteen
      716859 = seven hundred and sixteen thousand, eight hundred and fifty-nine
           5 = five
     3755909 = three million, seven hundred and fifty-five thousand, nine hundred and nine
  8103390315 = eight billion, one hundred and three million, three hundred and ninety thousand, three hundred and fifteen
      963749 = nine hundred and sixty-three thousand, seven hundred and forty-nine
    65171203 = sixty-five million, one hundred and seventy-one thousand, two hundred and three
        9266 = nine thousand, two hundred and sixty-six
       63531 = sixty-three thousand, five hundred and thirty-one
```

### Quiz

Ask interactive arithmetic questions with encouraging responses.

```
$ python -m mathfun.quiz
╔═══════════════════════════════════════════════════════════════════════════════════════════╗
║                                      What is 9 - 5?                                       ║
╚═══════════════════════════════════════════════════════════════════════════════════════════╝
(f) 1     (g) 3     (h) 4    
Answer? h
That's correct 😎
╔═══════════════════════════════════════════════════════════════════════════════════════════╗
║                                      What is 2 + 1?                                       ║
╚═══════════════════════════════════════════════════════════════════════════════════════════╝
(f) 3     (g) 3     (h) 4    
Answer? 3
Good job 🤩
╔═══════════════════════════════════════════════════════════════════════════════════════════╗
║                                      What is 3 - 3?                                       ║
╚═══════════════════════════════════════════════════════════════════════════════════════════╝
(f) 0     (g) 0     (h) 0    
Answer? a
Expected a number or an alphabet: f, g, h. Try again.
Answer? g
Well done 😄
╔═══════════════════════════════════════════════════════════════════════════════════════════╗
║                                      What is 9 + 0?                                       ║
╚═══════════════════════════════════════════════════════════════════════════════════════════╝
(f) 3     (g) 4     (h) 9    
Answer? 10
Oopsie daisy ⁉
You chose 10. It should have been 9
╔═══════════════════════════════════════════════════════════════════════════════════════════╗
║                                      What is 10 + 7?                                      ║
╚═══════════════════════════════════════════════════════════════════════════════════════════╝
(f) 16     (g) 17     (h) 15    
Answer? g
Yay 🥳
```

## Wishlist

- Property testing and example based testing.
- Generalize code, if possible

## Contributing

Clone the repository and prepare for development:

```
cd mathfun/

python -m venv venv
source venv/bin/activate

pip install flit
flit install --symlink --extras tests
```

Start hacking. Patches and pull-requests welcome. Make sure tests pass; at
least a handful of passes (we use property-based testing using `hypothesis`):

```sh
pytest --count 42
```
